# Makefile für das Projekt MFV-Wählscheibe
# Für avr-gcc; ich benutze WinAVR 2010.
# Erstellen mit "make", programmieren mit avrpp mit "make program".
# Schaltung zum Einbau in alte Telefone,
# ermöglicht Tonwahl trotz Fingerlochscheibe
# + programmierbare Kurzwahl (22 Nummern à 20 Ziffern)
# + CLIP / Rückruf / akustische Anzeige der entgangenen Anrufe,
# + Autowahl, + Wählen bei aufgelegtem Telefonhörer.
# $P = Projektname: Apparateeinbau, nsi und nsa in Reihe
# $D = Mikrocontroller ATtiny45 oder ATtiny85
# $F = Resonatorfrequenz zwischen 3 und 12 MHz, ideal 4 MHz

P = mfv2c
D = attiny45
F = 4000000
COMPILE = avr-gcc -Wall -Wno-parentheses -Os -mmcu=$D -DF_CPU=$F
AVRDUDE = avrdude -c pony-stk200 -P lpt1 -p $D -E noreset

all: $P.elf $P.lst

$P.elf: $P.c clip.S Makefile
	$(COMPILE) -o $@ $(filter %.c %.cpp %.S,$^)
	@avr-size -C --mcu=$D $@

# Nur für avrdude erforderlich
$P.hex: $P.elf
	avr-objcopy -j .text -j .data -O ihex $< $@
# -j .eeprom -j .fuse -j .signature

$P.lst: $P.elf
	avr-objdump -d $< > $@
	avr-strip $<

.PHONY: clean program fuse flash
clean:
	-rm -rf *.elf *.lst

#avrpp ist meine Erweiterung von ElmChans Parallel-Programmer
#(bei identischer Hardware) und liest elf-Dateien
program: $P.elf
	avrpp -ff $<

# zum Aktualisieren der Firmware bei gleich bleibenden Fuses
flash: $P.elf
	avrpp $<

#avrdude kann mit dem o.a. „pony-stk“ nur einmal verwendet werden,
#wenn Bit 7 von HFUSE gelöscht ist = RSTDISBL (Reset Disable)
program-avrdude: $P.hex
	$(AVRDUDE) -U flash:w:$<:i -U lfuse:w:0x5C:m -U hfuse:w:0x57:m

# zum Aktualisieren der Firmware bei gleich bleibenden Fuses
# (unmöglich mit Low-Voltage-ISP)
flash-avrdude: $P.hex
	$(AVRDUDE) -U flash:w:$<:i
