# Convert pulse dialing to dual-tone multi-frequency dialing

Modify an old telephone to convert from pulse to multi-frequency dialing.

## Original source

- https://www-user.tu-chemnitz.de/~heha/basteln/Haus/Telefon/Impulswahl%e2%86%92DTMF/

## Prerequisites

- phone with pulse dial
- I build the mfv2b circuit with an ATTiny45 instead of the mfv2c

![mfv2b-schematic](https://www-user.tu-chemnitz.de/~heha/basteln/Haus/Telefon/Impulswahl%E2%86%92DTMF/mfv.zip/mfv2b/mfv2b.wmf?as=SVG)*mfv2b-schematic*

- I used guloprog to flash the image (https://guloshop.de/f/guloprogS_doc.pdf)
- a Linux machine (since guloprog does not run under Windows out-of-the-box) with the following tools:

```
apt-get install avrdude
apt-get install gcc-avr
apt-get install avr-libc
```

## Compile

```
make
```

## Flash

```
avrdude -p attiny45 -c usbasp -Uflash:w:./mfv2c.elf:e
```

## Fuse

- low fuse for external resonator 4 MHz (http://www.engbedded.com/fusecalc/)

```
avrdude -p attiny45 -c usbasp -U lfuse:w:0x5c:m
```
